fs = require 'fs'

class User
  constructor: (@from) ->
    @id = @from.id
    @firstName = @from.first_name
    @lastName = @from.last_name
    @username = @from.username
    @name = @username || (@firstName + ' ' + @lastName)
    @getLevel()

User::getLevel = ->
  try 
    users = JSON.parse fs.readFileSync "users.json"
    unless users[@id]
      @level = 0
      do @saveLevel
      return @level
    @level = users[@id]
  catch e
    console.error "Couln't receive users' levels; #{e.message}"

User::saveLevel = ->
  try
    users = JSON.parse fs.readFileSync "users.json"
    users[@id] = @level
    fs.writeFileSync "users.json", JSON.stringify users
  catch e
    console.error "Couln't save users' levels; #{e.message}"

User::nextLevel = ->
  @level++
  do @saveLevel

User::isQuited = ->
  @level == -1

User::quit = ->
  @level = -1
  do @saveLevel

User::toString = ->
  "#{@name}(##{@id}) level #{@level}"

User.find = (from) ->
  new User from

module.exports = User