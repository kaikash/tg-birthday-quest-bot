module.exports =
  token: process.env.TELEGRAM_TOKEN
  proxy:
    socksHost: process.env.PROXY_SOCKS5_HOST
    socksPort: parseInt(process.env.PROXY_SOCKS5_PORT)
    socksUsername: process.env.PROXY_SOCKS5_USERNAME
    socksPassword: process.env.PROXY_SOCKS5_PASSWORD