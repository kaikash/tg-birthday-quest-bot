TelegramBot = require 'node-telegram-bot-api'
Agent = require 'socks5-https-client/lib/Agent'
User = require './User.coffee'

levels = require './configs/levels.coffee'
helpers = require './helpers.coffee'

class QuestBot
  constructor: (@options) ->
    @token = @options.token
    @proxy = @options.proxy
    @bot = new TelegramBot @token, 
      polling: true
      request: 
        agentClass: Agent,
        agentOptions: @proxy

QuestBot::initListeners = ->
  @bot.on 'message', (msg) =>
    try
      if msg.text
        @handleMessage msg
      else
        @unknownMessage msg
    catch err
      console.error err

QuestBot::run = ->
  do @initListeners

QuestBot::unknownMessage = (msg) ->
  @bot.sendMessage msg.chat.id, helpers.getMessage('unknownMessage')

QuestBot::handleMessage = (msg) ->
  user = User.find msg.from
  console.log "new message from #{user}: #{msg.text}"

  if msg.text == '/start'
    @sendMessage user, msg.chat.id, helpers.getMessage('hello', {name: user.name})
    return

  if user.isQuited()
    @sendMessage user, msg.chat.id, helpers.getMessage('alreadyQuited')
    return

  if user.level == 4 && msg.text.toLowerCase() == 'синяя'
    user.quit()
    return

  if helpers.isCompleted user.level
    @sendMessage user, msg.chat.id, helpers.getMessage('alreadyCompleted')
    return

  if helpers.isCorrect user.level, msg.text
    if levels[user.level].message
      @sendMessage user, msg.chat.id, helpers.getMessage("msg.level", {level: user.level})
    if levels[user.level].photo
      @bot.sendPhoto msg.chat.id, levels[user.level].photo

    do user.nextLevel
  else
    @sendMessage user, msg.chat.id, helpers.getMessage('wrongMessage')

QuestBot::sendMessage = (user, chatId, msg) ->
  @bot.sendMessage chatId, msg
  console.log "sending new msg to #{user}: #{msg}"


module.exports = QuestBot








