messages = require './configs/messages.coffee'
levels = require './configs/levels.coffee'

getMessage = (msg, extra) ->
  if msg == 'msg.level' && (typeof extra.level == 'number') && levels[extra.level]
    return levels[extra.level].message

  if Array.isArray messages[msg]
    ret = messages[msg][Math.floor(Math.random() * messages[msg].length)]
  else
    ret = messages[msg] || messages['unknownMessage']
  for key of extra
    ret = ret.replace "{{#{key}}}", extra[key]
  ret

isCorrect = (level, msg) ->
  if isCompleted level
    return false

  if Array.isArray levels[level].passcode
    for _msg in levels[level].passcode
      if _msg.toLowerCase() == msg.toLowerCase()
        return true

  if (typeof levels[level].passcode == 'string') && (levels[level].passcode.toLowerCase() == msg.toLowerCase())
    return true

  false

isCompleted = (level) ->
  level >= levels.length


module.exports = 
  getMessage: getMessage
  isCorrect: isCorrect
  isCompleted: isCompleted